Demonstration of finding plane patches within a voxel binned point cloud.

Requires: python python-numpy python-vtk python-mayavi2

Author: Julian Ryde

One possible way to run the code is via the following commands.

    cd /tmp
    git clone https://jryde@bitbucket.org/jryde/test.git
    cd test
    python voxel_planes.py
