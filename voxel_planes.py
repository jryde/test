#! /usr/bin/python
''' Demonstration of finding plane patches within a voxel binned point cloud.
Requires: python python-numpy python-vtk python-mayavi2

Author: Julian Ryde
'''
import numpy as np

import visualiser
import data
import cube

def pca(xyzs): # TODO this function is copied from pca_edge
    '''Returns the centroid and eignvalues of PCA of the zero meaned xyzs.
    xyzs is an N by 3 numpy array of points'''
    # should I use matplotlib.mlab.PCA(a) instead?
    assert len(xyzs) > 2, 'Need at least 3 points for SVD'
    centroid = xyzs.mean(axis=0)
    U, S, V = np.linalg.svd(xyzs - centroid)
    # eigenvalues S
    # eigenvectors V, I think they are arranged horizontally namely each row of
    # V is an eigenvector
    return centroid, S, V

# TODO merge with BinnedPointcloud class in pca_edge.py
class BinnedPointCloud:
    ''' A dense grid where each grid cell contains a list of points'''
    ev_dt = np.dtype([('centroid', '<f4', (3, )),
                      ('evals', '<f4', (3, )), 
                      ('evecs', '<f4', (3, 3))]) 
    # TODO add keep points flag to disable self.bins
    # TODO should I store an array in each bin?
    def __init__(self, mins, maxs, resolution):
        ''' mins are the minimum x, y, z and maxs are the max, resolution '''
        self.mins, self.maxs = np.array(mins), np.array(maxs)
        self.res = resolution
        D = np.int32((self.maxs - self.mins)/ resolution)
        assert D.prod() < 100e6, 'Array too large ' + str(D)
        self.og = np.zeros(D, dtype=np.int32)
        self.bins = np.zeros(D, dtype=np.object)

    def __repr__(self):
        return str(self.og.shape) + ', resolution: ' + str(self.res) \
                + ', bounds: ' + str(self.mins) + str(self.maxs)
    
    def count_bins(self, occupancy_threshold=0):
        return int(np.sum(self.og > occupancy_threshold))

    def get_inds(self, occupancy_threshold=0):
        inds = np.array(np.where(self.og > occupancy_threshold)).T
        return inds

    def get_iterator(self, occupancy_threshold=0):
        ''' Returns lists of points for all cells with occupancy (point count)
        greater than the specified threshold  '''
        # np.where does not seem to work with self.coarse when it is assigned???
        inds = self.get_inds(occupancy_threshold)
        for row in inds:
            yield self.bins[row[0], row[1], row[2]]

    def unquantize(self, inds):
        ''' Returns the xyz value corresponding to the array index for this
        occupancygrid'''
        return inds * self.res + self.mins

    def quantize(self, xyzs):
        ''' Returns the array indices for the Nx3 xyz points and so should
        always be positive '''
        return np.int32((xyzs - self.mins)/self.res)

    def addpoints(self, xyzs):
        inds = self.quantize(xyzs)

        added = 0
        # TODO do this in an faster array like manner perhaps with sorting?
        for xyz, ind in zip(xyzs, inds):
            try:
                self.og[ind[0], ind[1], ind[2]] += 1
            except IndexError as ie: 
                # reject points outside the occupancy grid
                #print ie
                continue
            added += 1
            if self.bins[ind[0], ind[1], ind[2]] == 0:
                self.bins[ind[0], ind[1], ind[2]] = list()
            self.bins[ind[0], ind[1], ind[2]].append(xyz)

        return added
        # return the number of points added?

    def display(self):
        # TODO different random colour for each cell of points?
        xyzs = []
        colors = []
        for i in self.get_iterator():
            C = np.tile(np.random.random(3), (len(i), 1))
            #C = np.tile(np.random.random(), len(i))
            colors.extend(C)
            xyzs.extend(i)
        colors = np.array(colors)
        xyzs = np.array(xyzs)

        visualiser.show_points(xyzs, colors)
        #visualiser.show_window()

    def planes3(self):
        ''' Consider the vertices of the grid.  For each vertex fit to 2x2x2
        neighbourhood around each vertex but clip plane to single voxel
        centered on vertex.'''

        offsets = np.indices((2, 2, 2))
        offsets.shape = 3, -1
        offsets = offsets.T

        occupancy_threshold = 2
        inds = bpc.get_inds(occupancy_threshold)

        # generate the grid vertices that need considering
        # remove duplicates
        # TODO this method is likely to be slow
        unique_inds = set()
        for ind in inds:
            for row in ind + offsets:
                unique_inds.add(tuple(row))
        unique_inds = np.array(tuple(unique_inds))

        EV = np.zeros(len(unique_inds), self.ev_dt)

        for i, vertex in enumerate(unique_inds): # for each grid vertex 
            # retrieve the points in the 2x2x2 array surrounding this vertex
            xyzs = []
            for offset in offsets:
                cell = vertex - offset
                try:
                    cell_points = self.bins[cell[0], cell[1], cell[2]]
                    if cell_points != 0:
                        xyzs.extend(cell_points)
                except IndexError: # offset outside of array
                    continue
            xyzs = np.array(xyzs) 
            # check there are points near enough to this vertex within +/-
            # resolution/2 
            vertex_pos = self.unquantize(vertex)
            near_inds = (xyzs < vertex_pos + self.res/2) & (xyzs > vertex_pos - self.res/2)
            near_inds = np.all(near_inds, axis=1)
            if not np.any(near_inds): 
                #import pdb; pdb.set_trace()
                continue # skip if there are no points

            # Now pca on the points 
            centroid, evals, evecs = pca(xyzs)
            EV[i] = centroid, evals, evecs

        # convert inds to positions
        positions = self.unquantize(unique_inds)
        # return the vertices and their associated pca results from the points
        # in the 2x2x2 voxels surrounding the grid intersection vertex
        return positions, EV

    def planes2(self):
        ''' Sliding window approach For each voxel take all eight possible
        overlapping 2x2x2 cells of points.  For the 2x2x2 cell that contains
        the largest number of points calculate pca fit and display it as fit
        for that voxel '''
        offsets = np.indices((2, 2, 2))
        offsets.shape = 3, -1
        offsets = offsets.T

        occupancy_threshold = 2
        len_bpc = self.count_bins(occupancy_threshold)
        EV = np.zeros(len_bpc, self.ev_dt)
        for i, inds in enumerate(self.get_inds(occupancy_threshold)):
            max_occupancy = 0
            for offset in offsets:
                cell = inds + offsets - offset
                try:
                    cell_occupancy = self.og[cell[:, 0], cell[:, 1], cell[:, 2]].sum()
                except IndexError: # offset outside of array
                    continue

                if cell_occupancy > max_occupancy:
                    max_occupancy = cell_occupancy
                    max_inds = cell
            xyzs = []
            # Now pca on the points from the 2x2x2 cell with highest occupancy
            for bin_xyzs in self.bins[max_inds[:, 0], max_inds[:, 1], max_inds[:, 2]]:
                if bin_xyzs != 0:
                    xyzs.extend(bin_xyzs)
            xyzs = np.array(xyzs) 
            centroid, evals, evecs = pca(xyzs)
            EV[i] = centroid, evals, evecs
        return EV

    def planes(self):
        print 'Fitting planes'
        occupancy_threshold = 2 # three points needed for svd
        # TODO tidy up this iteration mechanism
        len_bpc = self.count_bins(occupancy_threshold)
        EV = np.empty(len_bpc, self.ev_dt)
        # only process bins with enough points
        for i, voxel_points in enumerate(self.get_iterator(occupancy_threshold)):
            voxel_points = np.array(voxel_points)
            centroid, evals, evecs =  pca(voxel_points)
            EV[i] = centroid, evals, evecs
        return EV

if __name__ == '__main__':
    res = 0.05
    # TODO adjust dimensions to the min and max of data points?
    R = 5
    visualiser.clear_window()
    # evals are the variance in the direction of the associated eigenvector
    eval_threshold = 0.2**2 

    # EV has already been calculated skip these for speed
    bpc = BinnedPointCloud((-R, -R, -R), (R, R, R), resolution=res)
    xyzs = data.get_test_points('car') # car, scan000

    # reduce number of points to speed up running for debug 
    lower, upper = (-1, -1, -1), (5, 1, 1)
    filter_inds = np.all((xyzs > lower) & (xyzs < upper), axis=1)
    #xyzs = xyzs[filter_inds]

    print 'Loading points into binned point cloud...'
    added = bpc.addpoints(xyzs) # TODO accelerate point loading
    print 'Points added:', added
    #bpc.display()
    #EV = bpc.planes()
    #EV = bpc.planes2()
    vertices, EV = bpc.planes3()

    print 'eigenvalue threshold', eval_threshold
    # classify into planes cylinders and other
    large_count = np.sum(EV['evals'] > eval_threshold, axis=1)
    # large_count == 0 # small point clusters 
    # large_count == 1 # cylinders
    # large_count == 2 # planes 
    # large_count == 3 # spheres/blobs 

    # draw planes
    # For the plane normal take the min eigen value and associated eigen vector
    # position vector followed by normal vector
    # they seem to be always sorted in descending order?
    #min_inds = np.argmin(EV['evals'], axis=1) 
    #plane_EV = EV[large_count == 2]
    plane_EV = EV
    normals = plane_EV['evecs'][:, 2, :] # minimum is last one

    cube.show_bounded_planes2(plane_EV['centroid'], normals, vertices, res)
    #visualiser.show_quads(plane_EV['centroid'], normals, size=res)

    # draw cylinders
    #cylinder_EV = EV[large_count == 1]
    #positions = cylinder_EV['centroid']
    #directions = cylinder_EV['evecs'][:, 0, :] # maximum is first one
    #radii = 0.5 * np.sqrt(cylinder_EV['evals'][:, 1]) # radius is second largest?
    #visualiser.cylinders(positions, directions, radii, length=res)

    visualiser.show_window()
    visualiser.clear_window()
