from tvtk.api import tvtk # TODO quicker if you only import needed ones? No it's not
import numpy as np
import matplotlib.mlab as mlab

''' vtk visualisation methods
Author: Julian Ryde, Vikas Dhiman
'''

def show_points(xyzs, colors=None):

    polydata = tvtk.PolyData()
    mapper = tvtk.PolyDataMapper()
    mapper.input = polydata

    #zMin = xyzs[:, 2].min()
    #zMax = xyzs[:, 2].max()
    #mapper.scalar_range = (zMin, zMax)
    #mapper.scalar_visibility = 1

    actor = tvtk.Actor()
    
    actor.property.point_size = 2
    actor.mapper = mapper
    renderer.add_actor(actor)

    polydata.points = xyzs

    # http://www.vtk.org/Wiki/VTK/Tutorials/GeometryTopology
    # VTK strongly divides GEOMETRY from TOPOLOGY. What most users would
    # think of as "points" are actually "points + vertices" in VTK. The
    # geometry is ALWAYS simply a list of coordinates - the topology
    # represents the connectedness of these coordinates. If no topology at
    # all is specified, then, as far as VTK is aware, there is NOTHING to
    # show. If you want to see points, you must tell VTK that each point
    # is independent of the rest by creating a vertex (a 0-D topology) at
    # each point.
    verts = np.arange(0, xyzs.shape[0], 1, 'l') # point ids
    verts.shape = (xyzs.shape[0], 1) # make it a column vector
    polydata.verts = verts

    if colors is None:
        depth = xyzs[:, 2]
    else: 
        depth = colors

    polydata.point_data.scalars = depth
    polydata.point_data.scalars.name = 'scalars'

def clear_window():
    global renderer
    renderer = tvtk.Renderer()

# TODO refactor renderer into a class variable to allow multiple displays
renderer = tvtk.Renderer()
def show_window(size=(640, 480)):
    # Renderer
    #renderer = tvtk.Renderer()
    #renderer.add_actor(actor)
    renderer.add_actor(tvtk.AxesActor()) # add axes

    renderer.background = (0.9, 0.9, 0.8)
    renderer.reset_camera()

    # Render Window
    renderWindow = tvtk.RenderWindow()
    renderWindow.add_renderer(renderer)

    # Interactor
    renderWindowInteractor = tvtk.RenderWindowInteractor()
    renderWindowInteractor.render_window = renderWindow
    renderWindowInteractor.interactor_style = tvtk.InteractorStyleTrackballCamera()

    # Begin Interaction
    renderWindow.render()
    renderWindow.size = size
    #print 'Window size:', renderWindow.size
    renderWindowInteractor.start()

def visualise(polydata):
    # Visualize
    mapper = tvtk.PolyDataMapper()
    mapper.input = polydata
     
    actor = tvtk.Actor()
    actor.mapper = mapper
     
    renderer.add_actor(actor)


### from https://svn.enthought.com/enthought/browser/trunk/src/lib/enthought/tvtk/examples/animated_texture.py?rev=9564 ###
# Read the texture from image and set the texture on the actor.  If
# you don't like this image, replace with your favorite -- any image
# will do (you must use a suitable reader though).
def vtkimage_from_array(ary):
    """ Create a VTK image object that references the data in ary.
        The array is either 2D or 3D with.  The last dimension
        is always the number of channels.  It is only tested
        with 3 (RGB) or 4 (RGBA) channel images.
       
        Note: This works no matter what the ary type is (accept
        probably complex...).  uint8 gives results that make since
        to me.  Int32 and Float types give colors that I am not
        so sure about.  Need to look into this...
    """
       
    sz = ary.shape
    dims = len(sz)
    ary = ary[::-1, ...]
   
    if dims == 2:
        # Image must be grayscale
        dimensions = sz[1], sz[0], 1       
        scalars = ary.ravel()
       
    elif dims == 3:
        # 3D numpy array is a 2D array of multi-component scalars.
        dimensions = sz[1], sz[0], 1
       
        # create a 2d view of the array
        scalars = ary.reshape((sz[0]*sz[1], sz[2]))
       
    else:
        raise ValueError, "ary must be 3 dimensional."
    return scalars, dimensions

def show_img(img):
    img_data = tvtk.ImageData(spacing=(1, 1, 1), origin=(0, 0, 0))
    img_data.point_data.scalars, img_data.dimensions = vtkimage_from_array(img)

    img_actor = tvtk.ImageActor(input=img_data)
    img_actor.rotate_wxyz(45, 1, 0, 0)

    renderer.add_actor(img_actor)

def show_quads(positions, directions, size):
    ''' Represents points and normals as small plane patches (quads)
    is positions is plane centre and directions is plane normals '''
    D = size # size of plane patches
    #pts = np.array( [ (0, 0, 0), (X, 0, 0), (X, X, 0), (0, X, 0) ] )
    #quads = np.array( [ (0, 1, 2, 3), (0, 1, 2, 3)] )

    # TODO convert loop to array operations
    pts = []
    for pos, direction in zip(positions, directions):
        direction = direction / np.linalg.norm(direction) # normalize direction vector

        # calculate the vectors U, V which are in plane vectors perpendicular
        # to the normal and projected into the xz, yz and xy planes
        UVW = 0.5*D*np.array([(direction[2] , 0             , -direction[0]),
                              (0            , direction[2]  , -direction[1]),
                              (direction[1] , -direction[0] , 0),
                              ])

        lens = mlab.vector_lengths(UVW, axis=1)
        # one of them might be very short so use the other two
        min_ind = np.argmin(lens)
        UVW = np.delete(UVW, min_ind, axis=0)
        U = UVW[0]
        V = UVW[1]
        # normalize
        U /= np.linalg.norm(U)
        V /= np.linalg.norm(V)
        C = np.cross(U, V) # should equal direction
        #import pdb; pdb.set_trace()

        # Create four points (must be in counter clockwise order)
        pts.append(pos - U - V)
        pts.append(pos + U - V)
        pts.append(pos + U + V)
        pts.append(pos - U + V)

    pts = np.array(pts)
    pointids = np.arange(pts.shape[0], dtype=np.int)
    quads = pointids.reshape((-1, 4))

    # Create a polydata to store everything in
    polydata = tvtk.PolyData(points=pts, polys=quads)

    # TODO this section has been copy pasted merge at some point
    # Set colors from the vector direction
    magnitudes = mlab.vector_lengths(directions, axis=1).reshape((-1, 1))
    colors = np.abs(directions / magnitudes) * 255
    colors = colors.astype(np.uint8)
    polydata.cell_data.scalars = colors
    polydata.cell_data.scalars.name = 'colors'

    visualise(polydata)

def quiver(positions, directions):
    ''' lines is nx6 numpy array with each row containing position and
    direction  x, y, z, nx, ny, nz '''

    positions = np.array(positions, dtype=np.float32) # cast all arrays to float
    directions = np.array(directions, dtype=np.float32) # cast all arrays to float

    # Create a vtkPoints object and store the points in it
    pts = np.vstack((positions, positions + directions))

    # Create a cell array to store the lines 
    pointids = np.arange(pts.shape[0], dtype=np.int)

    # lines is nx2 array with each row containing 2 points to be connected
    lines = pointids.reshape((2, -1)).T
     
    # Create a polydata to store everything in
    linesPolyData = tvtk.PolyData()
    # Add the points to the dataset
    linesPolyData.points = pts
    # Add the lines to the dataset
    linesPolyData.lines = lines
     
    # Set line colors
    # color from the vector direction
    magnitudes = mlab.vector_lengths(directions, axis=1).reshape((-1, 1))
    colors = np.abs(directions / magnitudes) * 255
    colors = colors.astype(np.uint8)
    linesPolyData.cell_data.scalars = colors
    linesPolyData.cell_data.scalars.name = 'colors'
     
    visualise(linesPolyData)

def line():
    source = tvtk.LineSource()
    # mapper
    mapper = tvtk.PolyDataMapper()
    mapper.input = source.output
     
    # actor
    actor = tvtk.Actor()
    actor.property.line_width = 10
    actor.mapper = mapper
    renderer.add_actor(actor)

def cylinders(positions, directions, radii, length=1):
    for position, dir, radius in zip(positions, directions, radii):
        # create source
        source = tvtk.CylinderSource()
        #source.center = position
        source.radius = float(radius)
        source.height = length
        source.resolution = 5
        source.capping = 0 # 0 no caps, 1 capped

        # mapper
        mapper = tvtk.PolyDataMapper()
        mapper.input = source.output

        # actor
        actor = tvtk.Actor()
        actor.mapper = mapper

        # cylinder is aligned with y-axis so rotation axis is cross product
        # with target direction

        initial_axis = (0, 1, 0)
        axis = np.cross(initial_axis, dir)
        # Current orientation
        #w0, x0, y0, z0 = actor.orientation_wxyz
        # The angle of rotation in degrees. 
        w = np.arccos(np.dot(dir, initial_axis))
        w = np.degrees(w)
        # The rotation is in the plane of dir and [x0, y0, z0], the axis of
        # rotation should be normal to it
        #x, y, z = np.cross(dir, [x0, y0, z0])
        actor.rotate_wxyz(w, axis[0], axis[1], axis[2])
        actor.position = position

        renderer.add_actor(actor)

# source - geometric entity
# mapper - conversion of geometric entity to renderable representation
# actor - control appearance of renderable representation

if __name__ == '__main__':
    xyzs = np.random.random((10, 3)) * 10
    uvws = np.random.random((10, 3))
    print 'Points'
    colors = np.random.random(len(xyzs))
    show_points(xyzs, colors)

    #xyzs = np.array([ (1, 0, 0), (0, 1, 0), (0, 0, 1) ])
    #uvws = np.array([ (1, 0, 0), (0, 1, 0), (0, 0, 1) ])

    print 'Cylinders'
    cylinders(xyzs, uvws, 0.1*np.ones(len(xyzs)), length=1)

    #print 'Line'
    #line()

    print 'Quads'
    show_quads(xyzs, uvws, 1)

    print 'Quiver' 
    quiver(xyzs, uvws)
    show_window()

    clear_window()

    print 'Displaying image'
    im = np.random.randint(0, 255, (50, 100, 3))
    im = np.uint8(im)
    show_img(im)

    show_window()
