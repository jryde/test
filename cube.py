import numpy as np

import visualiser
from tvtk.api import tvtk
import matplotlib.mlab as mlab

class Cube:
    verts = np.array(
    [[0, 0, 0],
    [0, 0, 1],
    [0, 1, 0],
    [0, 1, 1],
    [1, 0, 0],
    [1, 0, 1],
    [1, 1, 0],
    [1, 1, 1]])

    edges = np.array([
    (0, 4),
    (4, 6),
    (6, 2),
    (2, 0),
    (0, 1),
    (4, 5),
    (6, 7),
    (2, 3),
    (1, 5),
    (5, 7),
    (7, 3),
    (3, 1),
    ])


# TODO refactor to use plane objects?
def intersect_plane_cube(normal, p0, cube_size, cube_center):
    # TODO add position and size of cube
    ''' plane has normal and a point p0, cube is axis aligned with position '''

    ''' For a line defined by p1, p2 and plane with normal n and point p0
    intersection is 
    u = n . (p0 - p1)
        -------------
        n . (p2 - p1)
    '''

    N = normal
    verts = Cube.verts * cube_size + cube_center - cube_size/2
    p1s = verts[Cube.edges[:, 0]] 
    p2s = verts[Cube.edges[:, 1]] 
    p1, p2 = verts[Cube.edges[0]]
    U = np.dot(p0 - p1s, N)/np.dot(p2s - p1s, N)
    U.shape = -1, 1 # make U column vector
    Ps = p1s + U*(p2s - p1s)
    #U = np.dot(p0 - p1, N)/np.dot(p2 - p1, N)
    # position of intersection is P = p1 + u(p2 - p1)
    # so for intersect with cube edge U is between 0 and 1
    inds = np.squeeze((U > 0) & (U < 1))
    return Ps[inds]

def triangulate(points):
    ''' Fills a set of convex planar points with triangles returning the
    points (in different order) and triangle indices '''

    # calculate point order 
    # The points all lie on plane so just need to consider the line joining a
    # random pair of points then order points by angle to this line as
    # calculated by dot product.

    # TODO reordering is only necessary for 5 or 6 points
    #if len(points) > 4:
    #P = points[np.lexsort(points.T)] # sort vertices by each axis this does not seem to work

    # pick random axis through polygon by selecting two points and then order
    # points by how far along this axis they are via the dot product which
    # gives the projection of line
    P = points
    ref_line = P[1] - P[0]
    projections = np.dot(P - P[0], ref_line)
    # re-order P according to projections
    #P = P[np.argsort(projections)]

    ref_line = P[1] - P[0]
    ref_line /= np.linalg.norm(ref_line)
    X = P[1:] - P[0]
    cos_thetas = np.dot(X, ref_line)/mlab.vector_lengths(X, axis=1)

    # sometimes cos(theta) is 1.00000002 and so inverse cos is invalid
    # enforce -1 to 1 range on cos_theta
    cos_thetas = np.clip(cos_thetas, -1, 1)

    thetas = np.arccos(cos_thetas)
    thetas = np.hstack((0, thetas))

    P = P[np.argsort(thetas)]

    # TODO this inds definition does not need to be done every time
    #inds = np.array([
        #(0, 1, 2), 
        #(1, 2, 3), 
        #(2, 3, 4), 
        #(3, 4, 5), 
        #])
    inds = np.array([
        (0, 1, 2), 
        (0, 2, 3), 
        (0, 3, 4), 
        (0, 4, 5), 
       ])
    inds = inds[:len(P) - 2] # only need 4 triangles to cover 6 points

    return P, inds

#n = (1, 1, 1)
#p0 = (0.5, 0.5, 0.5)
#p0 = (0.7, 0.7, 0.7)
# TODO should use vtk Polygon but cannot get it to work under tvtk

def show_bounded_planes2(positions, normals, cube_centers, cube_size):
    ''' Show the plane elements defined by positions and normals bounded by
    voxel of size resolution ''' 
    points = []
    triangles = []
    for p0, n, cube_center in zip(positions, normals, cube_centers):
        P = intersect_plane_cube(n, p0, cube_size, cube_center)
        #print p0, cube_center, P
        if len(P) < 3: continue
        P, inds = triangulate(P)
        triangles.extend(inds + len(points))
        points.extend(P)

    triangles = np.array(triangles)
    points = np.array(points)
    polydata = tvtk.PolyData(points=points, polys=triangles)
    visualiser.visualise(polydata)

def show_bounded_planes(positions, normals, resolution):
    ''' Show the plane elements defined by positions and normals bounded by
    voxel of size resolution ''' 

    print len(positions)
    points = []
    triangles = []
    for p0, n in zip(positions, normals):
        # quantise but this is position of bottom left of cube
        cube_center = np.round(p0/resolution) * resolution

        P = intersect_plane_cube(n, p0, resolution, cube_center)
        #print p0, cube_center, P
        if len(P) < 3: continue
        P, inds = triangulate(P)
        triangles.extend(inds + len(points))
        points.extend(P)

    triangles = np.array(triangles)
    points = np.array(points)
    polydata = tvtk.PolyData(points=points, polys=triangles)
    visualiser.visualise(polydata)

if __name__ == '__main__':
    count = 5
    positions = np.random.random((count, 3)) * 2
    normals = np.random.random((count, 3))
    #normals = np.array([(1, 1, 1), ])
    #positions = np.array([(0.4, 0.4, 0.4), ])

    visualiser.clear_window()
    show_bounded_planes(positions, normals, 0.2)
    visualiser.show_window()

#for i in range(10):
#    n = np.random.random(3)
#    p0 = np.random.random(3)
#
#    print 'Normal, point:', n, p0
#
#    P = intersect_plane_cube(n, p0)
#    P, inds = triangulate(P)
#    polydata = tvtk.PolyData(points=P, polys=inds)
#
#    visualiser.clear_window()
#    visualiser.visualise(polydata)
#    visualiser.show_window()
