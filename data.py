import numpy as np
import urllib2
import os

data_url = 'http://www.cse.buffalo.edu/~jryde/share/' 
tmp = '/tmp/'
# upload data to sol.cse.buffalo.edu:public_html/share
# name = '/jcr/data/car.npz'
# name = 'scan000.npz' # from thermolab dataset

def get_test_points(name='scan000'):
    ''' Valid names: scan000, car 
    All pointclouds should be in metres'''

    fname = name + '.npz'

    if not os.path.exists(tmp + fname):
        # get from website
        url = urllib2.urlopen(data_url + fname)
        localFile = open(tmp + fname, 'w')
        print 'Downloading test data:', url.geturl(), ' to ', tmp + fname
        localFile.write(url.read())
        localFile.close()

    xyzs = np.load(tmp + fname).items()[0][1]
    return xyzs

def convert_bremen_data(infname, outfname):
    # Extract x, y, and z columns from bremen city scan in txt file (infname)
    # Save extracted array to npz file (outfname)
    xyzs = np.loadtxt(infname, skiprows=1, usecols=(0, 1, 2))
    np.savez(outfname, xyzs)

def get_bremen_points():
    fname = '/data/bremen_city/scan000.npz' # Aligned points from ~1/2 of Bremen dataset
    print 'Loading bremen city data in ', fname
    scale = 1.0 # data in m?
    xyzs = np.load(fname).iteritems().next()[1] * scale 
    return xyzs
